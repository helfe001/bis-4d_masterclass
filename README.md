---
output: html_document
bibliography: BIS-4D_masterclass.bib
---

# BIS-4D Masterclass <img src="out/maps/target/SOM_per/dynamic/pdf_png_gif/time_series/m_SOM_per_pred50_d_5_15.gif" align="right" width="500"/>

**BIS-4D** is a high resolution modelling and mapping platform for the Netherlands in 3D space and time (3D+T). Using soil point data and spatially explicit environmental variables, or covariates, BIS-4D can predict basic soil chemical and physical properties at any location, depth and year in the Netherlands. Publications are available for maps of soil pH in 3D space ([Helfenstein et al., 2022](https://doi.org/10.1016/j.geoderma.2021.115659)), for maps of soil organic matter in 3D space and time ([Helfenstein et al., 2024a](https://doi.org/10.1038/s43247-024-01293-y)) and for updated versions of pH and SOM and seven other physical and chemical soil properties ([Helfenstein et al., 2024b](https://doi.org/10.5194/essd-2024-26)). The importance of BIS-4D for filling the data gap of key soil properties at high resolution with quantified accuracy is explained in this video: <https://www.youtube.com/watch?v=ENCYUnqc-wo>.

This repository was created for the masterclass "Digital Soil Mapping in 3D space and time: a hands on tutorial" at the 2023 Wageningen Soil Conference (WSC) to guide anyone interested in spatial modelling or digital soil mapping through the key steps of BIS-4D. We welcome anyone to test it and try it out also after the WSC, so that it may serve as a demo version of the BIS-4D model. The amount of soil point data used has been reduced to those that are publicly available and the spatial resolution has been aggregated to 1km resolution to reduce the computational demands so that R scripts should run on any standard computer.

## BIS-4D tutorial

The [00_TUTORIAL_guide](00_TUTORIAL_guide.html) serves as a guide through the main components of BIS-4D and is a good starting point. It acts as a guide for the complementary R script, [00_TUTORIAL_script.R](00_TUTORIAL_script.R), where users can run R code themselves. It is recommended to first spend time on this tutorial before diving into the other R scripts numbered 11-70 and the model input and output data. The tutorial should be self-explanatory and run on any OS (Windows, Mac, Linux).

## Model workflow (R scripts)

Going into more detail beyond the tutorial, below we summarize the main R scripts that make up the BIS-4D modelling and mapping platform. Note that some scripts, especially for the preparation of covariates (scripts 20-24), require other software besides R, mainly GDAL and SAGA. In addition, while BIS-4D is not OS dependent, it is generally run on Ubuntu and has not been fully tested for Windows and Mac. This might potentially only be an issue in a few very specific places, e.g. creating a GIF using bash (Unix shell) at the end of script [62](62_soil_maps_visualize_dyn.R).

### 1. Soil point data preparation

-   [11_soil_PFB_prep.R](11_soil_PFB_prep.R) - Connect to BIS (currently version 7.4) Oracle database (DB) using oracle client basic and odbc driver, query DB using dplyr syntax, compile relevant soil property data and geographic locations of sampling locations and export table of soil property data and x- and y-coordinates and depth information. BIS contains parts that are not publicly available so credentials are not given, however, code may still be useful for a general approach to reading data into R for data manipulation and statistical analysis from a database.
-   [12_soil_BIS_master_tibble.R](12_soil_BIS_master_tibble.R) - Combine all BIS soil information: in this case only PFB lab and field tibbles into one master tibble.
-   [15_soil_BIS_expl_analysis_target_pH.Rmd](15_soil_BIS_expl_analysis_target_pH.Rmd) - Exploratory analysis of soil pH point data in BIS
-   [16_soil_BIS_master_tibble_final.R](16_soil_BIS_master_tibble_final.R) - Based on findings in exploratory analysis of BIS data (scripts "15_soil_BIS_expl_analysis\_[].R"), remove any response data we do not want to include in BIS-3D models (e.g. O horizon / humus layer above mineral soil observations, any observations where we are certain that they are outliers)

### 2. Covariate preparation

-   [20_cov_prep_gdal.R](20_cov_prep_gdal.R) - Assemble & prepare predictors (covariates as raster data):
    -   Designate coordinate system (projection)
    -   Resample covariates so they have the same origin, cell locations and extent
    -   Mask nodata areas (water and areas outside NL) of continuous covariates (categorical covariates masked after reclassification in script [23_cov_cat_recl_gdal.R](23_cov_cat_recl_gdal.R))
-   [21_cov_dem_deriv_saga.R](21_cov_dem_deriv_saga.R) - Compute digital elevation model (DEM) derivatives using AHH2 (Dutch DEM, version 2) based on [Hengl and MacMillan (2019)](https://soilmapper.org/).
-   [22_cov_sensing_deriv.R](22_cov_sensing_deriv.R) - Compute spectral indices of remote sensing covariates:
    -   Define spectral indice functions according to [Loiseau et al., 2019](https://doi.org/10.1016/j.jag.2019.101905).
    -   Apply functions to remote sensing covariates, using R, G, B & NIR bands
    -   Compute PCA of all raw and processed (spectral indices) remote sensing data
-   [23_cov_cat_recl_gdal.R](23_cov_cat_recl_gdal.R) - Prepare categorical covariates:
    -   Define categorical variables in covariate stack as such
    -   Reclassify: combine levels of each categorical covariate into new levels that are more broad
    -   Designate NA values and mask categorical covariates
-   [24_cov_dyn_prep_gdal.R](24_cov_dyn_prep_gdal.R): Prepare and derive dynamic 2D+T and 3D+T covariates:
    -   define dynamic categorical variables in covariate stack as such
    -   reclassify: combine levels of each categorical dynamic covariate into new levels that are more broad
    -   designate NA values and mask dynamic categorical covariates
    -   create dynamic covariates for every year $t$ and in the case of 3D+T covariate for every year $t$ and every *GlobalSoilMap* (GSM; [Arrouays et al., 2014](https://books.google.nl/books?hl=en&lr=&id=S5ClAgAAQBAJ&oi=fnd&pg=PP1&dq=GlobalSoilMap:+Basis+of+the+Global+Spatial+Soil+Information+System&ots=iorqtfeNym&sig=NclsMkhU9ILP0RMX8P0fhvB4M90&redir_esc=y#v=onepage&q=GlobalSoilMap%3A%20Basis%20of%20the%20Global%20Spatial%20Soil%20Information%20System&f=false)) depth layer $d$ (or other chosen depth layer).
-   [25_cov_expl_analysis_clorpt.Rmd](25_cov_expl_analysis_clorpt.Rmd) - Exploratory analysis of covariates, inspecting a few of each CLORPT soil forming factor
-   [25_cov_expl_analysis_cont_cat.Rmd](25_cov_expl_analysis_cont_cat.Rmd) - Exploratory analysis of covariates: histogram, map and, if categorical, pie chart of classes of each covariate, grouped into continuous and categorical covariates

### 3. Overlay soil point data with covariates, regression matrix and covariate reduction

-   [30_regression_matrix_BIS.R](30_regression_matrix_BIS.R) - Read in prepared covariate stack and soil point data with coordinates; overlay rasters and points and prepare regression matrix (extract covariate values from sampled/observed locations)
-   [31_regression_matrix_RFE.R](31_regression_matrix_RFE.R) - variable/covariate reduction using de-correlation and recursive feature elimination (RFE):
    -   select TARGET variable (response), removing all rows with no observations for TARGET soil property
    -   if calibration data includes field estimates, remove field estimates from locations and horizons for which lab measurements are present
    -   RFE step 1: Remove covariates whose pearson correlation \> 0.85
    -   Prepare regression matrix for model calibration for iterative RFE: turn categorical covariates into factors, split into training and test data
    -   RFE step 2: RFE using `caret::rfe()` with OUTER and INNER k-fold CV using default RF ranger hyperparameters
-   [32_regression_matrix_dyn_4D.R](32_regression_matrix_dyn_4D.R) - derive dynamic 2D+T and 3D+T covariates at soil point calibration and validation locations:
    -   overlay rasters and points and prepare regression matrix (extract covariate values from sampled locations)
    -   derive covariates dynamic over 2D space & time (2D+T; "[]\_xyt\_[]") and 3D+T ("[]\_xydt\_[]") based on land use (LU) maps and maps of peat horizon occurrences
    -   change re-visited PFB sites from field campaign into validation locations
-   [35_model_data_expl_analysis_pH_KCl.Rmd](35_model_data_expl_analysis_pH_KCl.Rmd) - Exploratory analysis of all input data related to target soil property (soil pH) used for model calibration and validation
-   [35_model_data_expl_analysis_SOM.Rmd](35_model_data_expl_analysis_SOM.Rmd) - Exploratory analysis of all input data related to target soil property (soil pH) used for model calibration and validation

### 4. Model tuning and calibration (training)

-   [40_train_RF_hyperparameter_tuning.R](40_train_RF_hyperparameter_tuning.R) - Tune random forest (RF) hyperparameters:
    -   Read in BIS regression matrix and select target (response) variable
    -   Turn categorical variables into factors
    -   Split into training and test data
    -   Fit RF models all using a cross-validation (CV) grouped by location of the training data (requires `caret`, `CAST` and `ranger` pkg (`ranger` is preferable because much faster)
    -   Different RF models are fit using a full cartesian grid of hyperparameters (e.g. `ntree`, `mtry`, `node size`, `splitrule`, resampling type and size) and model performance values for each set of hyperparameters is saved
-   [41_train_QRF_optimal_model.R](41_train_QRF_optimal_model.R) - Calibrate/train quantile regression forest (QRF) with optimal hyperparameters:
    -   Read in regression matrix and tuning grid of hyperparameters specific to chosen target soil property (response/dependent var)
    -   Fit QRF model using a cross-validation (CV) grouped by location of the training data (requires `caret`, `CAST` and `quantregForest` or `ranger` pkg (`ranger` is preferable because much faster)) and optimal hyperparameters
    -   Save trained/fitted/calibrated model to disk

### 5. Model evaluation and map quality assessment

Model were evaluated using accuracy plots (i.e. predicted vs. observed) and the following metrics: mean error (**ME**), mean squared error (**MSE**), root mean squared error (**RMSE**), model efficiency coefficient (**MEC**) and the prediction interval coverage probability (**PICP**) of the 90^th^ prediction interval **PI90**. Note that BIS-4D also evaluates map quality using design-based inference using 2 independent probability sample for statistical validation ([Helfenstein et al., 2022](https://doi.org/10.1016/j.geoderma.2021.115659)). However, because these datasets are not publicly available, here we only evaluation map quality using non-design based inference, more specifically 10-fold cross-validation (CV) using a purposive sample and differences in the TARGET soil property at two points in time at approx. 70 separate locations.

-   [50_model_evaluation_all_depths.R](50_model_evaluation_all_depths.R) - evaluate map quality using statistical validation:
    -   use "ranger" pkg to predict mean and all quantiles
    -   evaluate map accuracy using hold outs from location-grouped 10-fold CV of calibration data
    -   make predicted vs. observed plots and accuracy metrics over all depths
    -   calculate PICP of all prediction intervals
-   [51_model_evaluation_depth_layers.R](51_model_evaluation_depth_layers.R) - same as above (script [50](50_model_evaluation_all_depths.R)), but separated by GSM depth layer
-   [52_model_evaluation_dyn_PFBval_2022.R](52_model_evaluation_dyn_PFBval_2022.R) - statistical validation of temporal changes in TARGET soil property:
    -   statistical validation of BIS-4D (3D+T) model predictions using PFB validation data, i.e. re-visited PFB sites of BIS in 2022
    -   Compute accuracy metrics and plots of difference in TARGET model predictions over time (delta TARGET)
-   [53_model_evaluation_var_imp_xML.R](53_model_evaluation_var_imp_xML.R) - Help explain/interpret ML/AI (xML/xAI) using 3 methods:
    1.  Variable importance (permutation or impurity)
    2.  Partial dependence plots (PDP): TARGET/response variable vs. predictors/covariates
    3.  Shapley values (used for local xML)

### 6. 3D+T soil prediction maps

-   [60_soil_maps_predict_QRF.R](60_soil_maps_predict_QRF.R) - Compute soil maps:
    -   Define target variable (response) and target prediction depth
    -   Read in fitted QRF model
    -   Read in stack of covariates (predictors)
    -   Predict response mean and quantiles (e.g. 50<sup>th</sup>/median, 5<sup>th</sup> & 95<sup>th</sup> quantiles) using terra::predict and ranger::predict arguments
    -   Calculate PI90
    -   Visualize maps and write rasters of results to disk
-   [61_soil_maps_predict_QRF_transect.R](61_soil_maps_predict_QRF_transect.R) - predict response mean and quantiles (e.g. 50<sup>th</sup>/median, 5<sup>th</sup> & 95<sup>th</sup> quantiles) across transect
-   [62_soil_maps_visualize_dyn.R](62_soil_maps_visualize_dyn.R) - plot and visualize TARGET prediction soil maps:
    -   Maps of mean and median predictions of first (1953) & last (2022) prediction year at each standard GSM depth
    -   Differences (delta TARGET) in mean and median predictions, respectively, between last and first year
    -   Maps of PI90 of first (1953) & last (2022) prediction year at each standard GSM depth
    -   Images (png) of each year of each standard GSM depth layer
    -   GIF animation of predictions at one GSM depth layer over all years

### 7. Time Series

-   [70_predict_dyn_time_series.R](70_predict_dyn_time_series.R) - predict and plot time series at validation locations revisited in 2022:
    -   derive dynamic covariate values at PFB validation sites for years 1953-2022
    -   predict for original sampling years (1950s-1990s)
    -   predict for year in which sites were re-visited (2022)
    -   predict mean, median, 5^th^ and 95^th^ quantile for every year at PFB validation sites
    -   time series plots of predicted TARGET values and point with lab measurements

## Summary of supporting scripts, files and directories

-   [R/other/](R/other/) - R scripts that are not part of core modelling workflow, i.e. do not need to be re-run when adding new data or changing model parameters.
    -   [fun_cov_dyn_xyt_xydt.R](R/other/fun_cov_dyn_xyt_xydt.R) - define functions to compute dynamic covariates for tabular and raster data:
        -   define function (fun/fnc) to compute dynamic land use (LU) covariate as a fnc of location ($x$,$y$) and time $t$:
            -   `LU_xyt_1km` (LU at time $t$)
            -   `LU_xyt_delta5_1km` (dominant LU during 5 years before $t$)
            -   `LU_xyt_delta10_1km` (dominant LU during 10 years before $t$)
            -   `LU_xyt_delta20_1km` (dominant LU during 20 years before $t$)
            -   `LU_xyt_delta40_1km` (dominant LU during 40 years before $t$)
        -   define function (fun/fnc) to compute dynamic peat covariate as a fnc of location ($x$,$y$) and time $t$: `peat[CLASS]\_xyt`
            -   fnc output is a fuzzy membership variable that can equal the following values depending on the 2D location and time (2D+T):
                -   0 = not CLASS
                -   1 = CLASS
                -   value btw. 0 & 1 = change from CLASS to not CLASS (or vice versa) where years between are interpolated with linear fnc
        -   define function (fun/fnc) to compute dynamic peat covariate as a fnc of location ($x$,$y$), depth $d$ and time $t$: `peat_xydt`
            -   fnc output is a fuzzy membership variable that can equal the following values depending on the 3D location and time (3D+T):
                -   0 = no peat
                -   1 = peat
                -   value btw. 0 & 1 = peat degradation / peat build-up where years between are interpolated with linear fnc
    -   [fun_partial_qrf.R](R/other/fun_partial_qrf.R) - Partial Dependence Plot for Quantile Random Forest; for dependency of any quantile in QRF adjusted fnc based on <https://github.com/vlyubchich/MML/blob/master/R/partial_qrf.R>
    -   [peat_xydt_visualization/](R/other/peat_xydt_visualization/) - Scripts to visualise peat categories with one peat horizon in different start- and ending depths, including the difference between two categories
    -   [predict_qrf_fun.R](R/other/predict_qrf_fun.R) - Modify tweaked version of ranger function so that mean can also be computed directly from QRF (slightly altered from [ISRIC's tweaked QRF predict function](https://git.wur.nl/isric/soilgrids/soilgrids/-/blob/master/models/ranger/predict_qrf_fun.R))
    -   [target_prediction_depth_rasters_GSM.R](R/other/target_prediction_depth_rasters_GSM.R) - Create target *GSM* prediction depth layers
-   [data](data/) - Input data for modelling workflow
    -   [covariates](data/covariates/) - metadata (e.g. README and reclassification table files) of covariates, which are based on the soil forming factors climate, geology, organism, relief and soil
        -   [climate](data/covariates/climate/) - README files ([...]\_readme.txt) of covariates related to soil forming factor climate
        -   [geology](data/covariates/geology/) - README, reclassification table ([...]\_reclassify.csv and [...]\_reclassify.xlsx) and attribute table of original classes ([...]\_attributes.csv) files of covariates related to soil forming factor geology/parent material
        -   [organism](data/covariates/organism/) - README, reclassification table, attribute table of original classes and any other metadata files of covariates related to soil forming factor organism (including land cover and land use)
        -   [relief](data/covariates/relief/) - README and reclassification table files of covariates related to soil forming factor relief/topography
        -   [soil](data/covariates/soil/) - README and reclassification table files of covariates related to soil maps
        -   [covariates_metadata.csv](data/covariates/covariates_metadata.csv) - Summary metadata table of all covariates used
        -   [covariates_metadata.xlsx](data/covariates/covariates_metadata.xlsx) - Summary metadata table of all covariates used
    -   [other/](data/other/) - Other spatial data not used as covariates (e.g. table of land use classes as designated in BIS database, shapefiles of provincial and country borders and probability sample strata and mapping mask used to assign "no data" values)
    -   [soil/](data/soil/field_campaign/) - soil point information of 2022 field campaign at revisited PFB locations
-   [out/](out/) - Intermediary (e.g. changes made to input data) and final model outputs
    -   [out/data/covariates/](out/data/covariates/) - intermediary and final covariates used for prediction, including GSM depth layer covariates
    -   [out/data/model/](out/data/model/) - tabular data (.Rds or .csv) on intermediary or final model output, e.g. regression matrices, hypertuning results, predictions vs. observed tables, QRF model fit object, etc.
    -   [out/data/soil/](out/data/soil/) - tabular data for soil property information from BIS database
    -   [out/figs/explorative](out/figs/explorative/) - Exploratory analysis and descriptive plots of modelling input soil point data
    -   [out/figs/models/](out/figs/models/) - Model evaluation plots: accuracy plots and metrics (ME, MSE, RMSE, MEC, PICP), variable importance measures and time series plots
    -   [out/maps/explorative](out/maps/explorative/) - Exploratory analysis maps of BIS soil point data, AHN and AHN derivatives
    -   [out/maps/target/](out/maps/target/) - GeoTIFFs, PDFs, PNGs and GIF files of TARGET prediction maps including PI90 for different years and depth layers
-   [img/](img/) - legend for SOM maps

## Funding

This project was financially supported by the Dutch Ministry of Agriculture, Nature and Food Quality ([WOT-04-013-010](https://research.wur.nl/en/projects/soil-property-mapping-wot-04-013-010) and [KB-36-001-014](https://research.wur.nl/en/projects/bro-soil-properties-analyses-spa-kb-36-001-014)).

## References

Arrouays, Dominique, Neil McKenzie, Jon Hempel, Anne Richer de Forges, and Alex McBratney, eds. 2014. *GlobalSoilMap: Basis of the Global Spatial Soil Information System*. Boca Raton: CRC Press Taylor & Francis Group.

Helfenstein, Anatol, Vera L. Mulder, Gerard B. M. Heuvelink, and Joop P. Okx. 2022. "Tier 4 Maps of Soil pH at 25 m Resolution for the Netherlands." *Geoderma* 410 (March): 115659. <https://doi.org/10.1016/j.geoderma.2021.115659>.

Helfenstein, A., Mulder, V.L., Heuvelink, G.B.M., Hack-ten Broeke, M.J.D., 2024a. Three-dimensional space and time mapping reveals soil organic matter decreases across anthropogenic landscapes in the Netherlands. *Communications Earth & Environment* 5, 1--16. <https://doi.org/10.1038/s43247-024-01293-y>

Helfenstein, A., Mulder, V. L., Hack-ten Broeke, M. J. D., van Doorn, M., Teuling, K., Walvoort, D. J. J., and Heuvelink, G. B. M.: BIS-4D: Mapping soil properties and their uncertainties at 25 m resolution in the Netherlands, Earth Syst. Sci. Data Discuss. [preprint], <https://doi.org/10.5194/essd-2024-26>, accepted, 2024b.

Hengl, Tomislav, and Robert A MacMillan. 2019. *Predictive Soil Mapping with R*. Wageningen, the Netherlands: OpenGeoHub foundation. <https://soilmapper.org/>

Loiseau, T., S. Chen, V. L. Mulder, M. Román Dobarco, A. C. Richer-de-Forges, S. Lehmann, H. Bourennane, et al. 2019. "Satellite Data Integration for Soil Clay Content Modelling at a National Scale." *International Journal of Applied Earth Observation and Geoinformation* 82 (October): 101905. <https://doi.org/10.1016/j.jag.2019.101905>.
