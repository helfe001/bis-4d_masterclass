#-------------------------------------------------------------------------------
# Name:     40_train_RF_hyperparameter_tuning.R
#           (RF = random forest)
#
# Content:  - read in TARGET soil property (response) regression matrix
#           - fit RF models all using a cross-validation (CV) grouped by
#             location of the training data (requires "caret", "CAST" and
#             "ranger" pkg ("ranger" is preferable because much faster)
#           - different RF models are fit using a full cartesian grid of hyper-
#             parameters (e.g. ntree, mtry, node size, splitrule, resampling
#             type and size) and model performance values for each set of hyper-
#             parameters is saved
#
# Refs:     - Cross validation strategies for spatio-temporal data using "CAST" pkg:
#             https://cran.r-project.org/web/packages/CAST/vignettes/CAST-intro.html
#             https://cran.r-project.org/web/packages/CAST/vignettes/AOA-tutorial.html
#           - Model tuning using "caret" pkg and RF hyperparameters:
#             https://topepo.github.io/caret/model-training-and-tuning.html
#             https://bradleyboehmke.github.io/HOML/random-forest.html#hyperparameters
#           
# Inputs:   - TARGET soil property regression matrix:
#             "out/data/model/[TARGET]/tbl_regmat_[TARGET].Rds"
#
# Output:   - table of model performance results of hyperparameter combinations:
#             out/data/model/[TARGET]/tbl_RF_hypertuning_[TARGET].Rds
#             out/data/model/[TARGET]/tbl_RF_hypertuning_[TARGET].csv
#
# Runtime:  - SOM lab measurements (15K obs), 28 covariates, 48 cores,
#             54 hyperparameter combinations: 12 min
#
# Project   BIS-4D Masterclass 
# Author:   Anatol Helfenstein
# Updated:  2023-07-07
#-------------------------------------------------------------------------------



### empty memory and workspace; load required packages -------------------------
gc()
rm(list=ls())

pkgs <- c("tidyverse", "ranger", "caret", "CAST")

lapply(pkgs, library, character.only = TRUE)



### Prepare modelling data -----------------------------------------------------

# 1) Specify DSM target soil property:
TARGET = "SOM_per"

# 2) Specify observation quality & dynamic or static model
OBS_QUAL = "_lab" # lab measurements & field estimates: ""; lab meas. only: "_lab"
TIME = "_dyn" # calibration 3D+T model using 2D+T & 3D+T dynamic covariates: "_dyn"
TIME_DIR = "dynamic" # either "static" or "dynamic"; directory to save plots

# 3) Specify number of cores/threads to use for parallel computation
THREADS = parallel::detectCores()
# use full computational power

# read in regression matrix of TARGET soil property
tbl_regmat_target <- read_rds(paste0("out/data/model/", TARGET, "/", TIME_DIR,
                                     "/tbl_regmat_", TARGET, TIME, ".Rds"))

# if model tuning should only include lab measurements, remove field estimates
if (OBS_QUAL == "_lab") {
  tbl_regmat_target <- tbl_regmat_target %>% 
    filter(!BIS_type == "field")
}

# Separate tables for calibration and validation data
tbl_regmat_target_cal <- tbl_regmat_target %>% 
  filter(split %in% "train")
tbl_regmat_target_val <- tbl_regmat_target %>% 
  filter(split %in% "test")



### Train RF model -------------------------------------------------------------

# RF hyperparameters (in e.g. "randomForest", "quantregForest" & "ranger" pkgs):
# 1)  Number of trees in the forest ("ntree" or "num.trees"):
#     although empirical evidence suggests that performance of prediction
#     remains good even when using only e.g. 100 trees, it should not be set too
#     small to ensure that every input gets predicted at least a few times.
# 2)  Number of predictors/covariates/features (P = total number of predictors)
#     to consider at any given split ("mtry"):
#     "quantregForest" & "randomForest" pkg default is 1/3 of P following
#     Breiman 2001 recommendation; "ranger" pkg default: rounded down square
#     root of P; for the purpose of tuning mtry parameter, Kuhn & Johnson 2013
#     recommend starting with 5 values of mtry that are evenly spaced between 2
#     and P.
# 3)  Complexity of each tree (e.g. "nodesize" = "min.node.size" and others):
#     - nodesize: fixes minimal number of instances in each terminal node,
#       determining how many observations at least lie in the same node;
#       default in "quantregForest" pkg = 10; default in "ranger" pkg 
#       "min.node.size" for regression = 5
#     - ranger::max.depth: Maximal tree depth. A value of NULL or 0 (default)
#       corresponds to unlimited depth, 1 to tree stumps (1 split per tree).
# 4)  Sampling scheme:
#     Default is bootstrapping where 100% of observations are sampled with
#     replacement; here, we can adjust both the sample size and whether to sample
#     with or without replacement. Decreasing the sample size leads to more diverse
#     trees and thereby lower between-tree correlation. If there are e.g. a few
#     dominating features in data set, reducing sample size can help minimize
#     between tree correlation. Also, when you have categorical predictors with
#     varying number of levels, sampling with replacement can lead to biased
#     variable split selection. Consequently, if you have categories that are not
#     balanced, sampling without replacement provides a less biased use of all
#     levels across the trees in the RF
#     - ranger::case.weights: Weights for sampling of training observations.
#     Observations with larger weights will be selected with higher probability
#     in the bootstrap samples for the trees.
# 5)  Splitting rule to use during tree construction ("splitrule"):
#     In "ranger", default splitrule is "variance"


# Model tuning: cross-validation & tuning using hyperparameters ----------------

# To prevent overfitting the model and improving separate & independent validation
# results later on, we make use of the "CAST" pkg of Hanna Meyer, embedded in a 
# "caret" framework. This allows machine learning (ML) for space-time data,
# for which models should be fit differently to non-spatio-temporal data...

# dataframe of covariate/predictors values at training locations
df_cov_train <- tbl_regmat_target_cal %>%
  dplyr::select(-c(split:hor, year), -all_of(TARGET)) %>% 
  as.data.frame() # "setting row names on tibble is deprecated"

# vector of response variable values (target soil property) at training locations
v_response <- tbl_regmat_target_cal %>%
  pull(all_of(TARGET))

# number of predictors/covariates used
P = ncol(df_cov_train)

# set seed to control randomness of cross-validation (CV)
set.seed(2022)

if (OBS_QUAL == "") {
  
  # index (row ID) of lab measurements
  v_index_lab <- tbl_regmat_target_cal %>% 
    rowid_to_column("index") %>% 
    filter(BIS_type %in% "lab") %>% 
    pull(index)
  
  # index (row ID) of field estimates
  v_index_field <- tbl_regmat_target_cal %>% 
    rowid_to_column("index") %>% 
    filter(BIS_type %in% "field") %>% 
    pull(index)
  
  # Use 10-fold CV grouped by location (site_id) using CAST::CreatSpacetimeFolds()
  # of only lab measurements
  indices <- CreateSpacetimeFolds(x = filter(tbl_regmat_target_cal, BIS_type %in% "lab"),
                                  spacevar = "site_id",
                                  k = 10)
  
  indices <- list(map(indices[[1]], ~c(v_index_lab[.x], v_index_field)),
                  map(indices[[2]], ~v_index_lab[.x])) %>% 
    set_names(c("index", "indexOut"))
  
} else {
  
  indices <- CreateSpacetimeFolds(x = tbl_regmat_target_cal,
                                  spacevar = "site_id",
                                  k = 10)
}

# list of case.weights to tune likelihood of lab measurements being chosen in
# bootstrap of calibration
ls_case_weights <- list(
  NULL,
  tbl_regmat_target_cal %>% 
    mutate(case_weights = if_else(BIS_type == "lab", 5, 1)) %>%
    pull(case_weights),
  tbl_regmat_target_cal %>% 
    mutate(case_weights = if_else(BIS_type == "lab", 10, 1)) %>%
    pull(case_weights)
)

# extensive tuning grid of hyperparameters
tbl_tuning <- expand.grid(num_trees = c(250, 500, 750),
                          mtry = round(P * c(.2, .25, .3)),
                          min_node_size = c(1, 3),
                          replace = c(TRUE, FALSE),
                          sample_fraction = c(.63, .8),
                          splitrule = "variance", # default
                          case_weights = ifelse(OBS_QUAL == rep("_lab", 3), 1, 1:3),
                          # cols to be filled in after training:
                          RMSE = NA,
                          Rsquared = NA,
                          folds_RMSE = NA,
                          folds_Rsquared = NA,
                          folds_MAE = NA,
                          folds = NA) %>%
  as_tibble() %>%
  # sample.fraction can only be tuned if replace = FALSE; change sample fraction
  # to 1 if replacement = TRUE and remove resulting duplicated rows
  mutate(sample_fraction = case_when(replace %in% TRUE ~ 1.0,
                                     replace %in% FALSE ~ sample_fraction)) %>%
  distinct()

# Control randomness of sampling in each fold in multi-core environment
set.seed(2022)
# 10 folds
seed_folds <- vector(mode = "list", length = length(indices$index))
for (i in 1:length(indices$index)) {
  seed_folds[[i]] <- sample.int(1000, nrow(tbl_tuning))
}
# For the final model:
seed_final <- sample.int(1000, 1)


# fitting/training/calibrating models ------------------------------------------
# full cartesian grid search by fitting model for ith hyperparameter combination
system.time(
  for(i in seq_len(nrow(tbl_tuning))) {
    
    # set nuances of model training:
    # By default, train chooses tuning parameter with best performance (e.g. if
    # chosen metric is RMSE, then it chooses model with lowest RMSE)
    # "one standard error" rule of Breiman et al. (1984) suggests that tuning
    # parameter associated with the best performance may overfit and so its
    # better to choose simplest model within one standard error (SE) of the
    # empirically optimal model; this is useful for simple trees that easily
    # overfit but little or no effect for RF...
    # "selectionFunction" only makes a difference when comparing models with
    # different hyperparameters within the same "train" object (unlike here...)
    train_control <- trainControl(method = "cv",
                                  # don't save predictions to save computation time
                                  savePredictions = FALSE,
                                  selectionFunction = "best", # or "oneSE"
                                  index = indices$index,
                                  # seeds for each set of hyperparameters
                                  seeds = c(map(seed_folds, ~ .x[i]),
                                            seed_final),
                                  allowParallel = THREADS)
    # indexFinal = optional vector of integers indicating which samples used to
    # fit the final model after resampling. If NULL, then entire dataset is used
    
    # set seed to control randomness of model training/fitting
    set.seed(2022)
    # There are rare cases where the underlying model function does not control
    # the random number seed, especially if the computations are conducted in C
    # code! Could this be the case for RF using ranger (written in C)?
    
    # train model using caret::train() & "ranger" implementation
    rf_cal <- train(x = df_cov_train,
                    y = v_response,
                    method = "ranger",
                    weights = ls_case_weights[[tbl_tuning$case_weights[i]]],
                    metric = "RMSE", # default for regression problems
                    num.trees = tbl_tuning$num_trees[i],
                    replace = tbl_tuning$replace[i],
                    sample.fraction = tbl_tuning$sample_fraction[i],
                    # Suffices to run standard RF instead of QRF for testing
                    # hyperparameters since we tune for mean predictions
                    # (I tested this and results are identical)
                    quantreg = FALSE, 
                    oob.error = FALSE, # to save computation time
                    num.threads = THREADS,
                    verbose = TRUE,
                    # seed = 2022,
                    tuneGrid = data.frame(
                      mtry = tbl_tuning$mtry[i],
                      splitrule = tbl_tuning$splitrule[i],
                      min.node.size = tbl_tuning$min_node_size[i]),
                    trControl = train_control)
    
    # export average RMSE & Rsquared over all folds & table of metrics per fold
    tbl_tuning$RMSE[i] <- rf_cal$results$RMSE
    tbl_tuning$Rsquared[i] <- rf_cal$results$Rsquared
    tbl_tuning$folds_RMSE[i] <- list(rf_cal$resample$RMSE)
    tbl_tuning$folds_Rsquared[i] <- list(rf_cal$resample$Rsquared)
    tbl_tuning$folds_MAE[i] <- list(rf_cal$resample$MAE)
    tbl_tuning$folds[i] <- list(rf_cal$resample$Resample)
  }
)
# time elapse:
#   - SOM lab measurements (15K obs), 28 covariates, 48 cores, 54 hyperparameter
#     combinations: 12 min

# Sort tibble of tuning results by best performance (lowest RMSE)
# However, if increase in RMSE is < 0.1%, then prioritize model with less trees
# and without case weights to decrease computation time
tbl_tuning <- tbl_tuning %>% 
  mutate(index = RMSE < (min(RMSE)+(min(RMSE)*0.001))) %>% 
  group_by(index) %>% 
  arrange(-index, num_trees, case_weights, RMSE) %>% 
  ungroup() %>%
  mutate(case_weights = case_when(case_weights == 1 ~ 1,
                                  case_weights == 2 ~ max(ls_case_weights[[2]]),
                                  case_weights == 3 ~ max(ls_case_weights[[3]])))

# save table of hypertuning results to disk
write_rds(tbl_tuning,
          paste0("out/data/model/", TARGET, "/", TIME_DIR, "/tbl_RF_hypertuning_",
                 TARGET, OBS_QUAL, TIME, ".Rds"))

# Flat files can't store the list columns
write_csv(tbl_tuning %>% 
            dplyr::select(num_trees:Rsquared),
          paste0("out/data/model/", TARGET, "/", TIME_DIR, "/tbl_RF_hypertuning_",
                 TARGET, OBS_QUAL, TIME, ".csv"))

# in case of varying hyperparameters, we can plot (using plot or ggplot)
# plot(rf_cal, metric = "RMSE")
# ggplot(rf_cal, metric = "Rsquared")
# "Randomly selected predictors" = mtry


