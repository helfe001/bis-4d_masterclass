#-------------------------------------------------------------------------------
# Name:     20_cov_prep_gdal.R
#           (cov = covariate, prep = preparation, gdal = GDAL)
#
# Content:  - assemble & prepare predictors (covariates as raster data):
#               - designate coordinate system (projection)
#               - resample covariates so they have the same CRS, origin & extent
#               - add Northing and Easting covariate
#               - mask nodata areas (water & areas outside NL) of continuous
#                 covariates (categorical covariates masked after reclassification
#                 in later script)
#               - assemble into raster stack and save
#           
# Inputs:   - covariate rasters (GeoTIFF (.tif)): data/covariates/
#           - covariate metadata ("data/covariates/covariates_metadata.csv")
#
# Temporary storage:  out/data/covariates/resample/  (GDAL in- and outputs)
#                     out/data/covariates/easting_northing/ (new rasters)
#                     out/data/covariates/mask_continuous/ (masked cont. cov)
#
# Output:   - covariate stack: out/data/covariates/20_r_stack_cov.grd
#
# Runtime:  - ca. 2 min
#
# Project:  BIS-4D Masterclass
# Author:   Anatol Helfenstein
# Updated:  June 2023
#-------------------------------------------------------------------------------



### Empty memory and workspace; load required packages -------------------------
gc()
rm(list=ls())

pkgs <- c("tidyverse", "here", "raster", "foreach", "truncnorm")
# "rgdal", "terra"
# As long as "terra" computations not serializable for parallel computation (see
# e.g. https://github.com/rspatial/terra/issues/36), we will use "raster" pkg

lapply(pkgs, library, character.only = TRUE)

# correct use of CRS according to PROJ 7 and higher, see e.g.:
# https://inbo.github.io/tutorials/tutorials/spatial_crs_coding/
# "raster" pkg still uses old strings even though we designate it correctly, so
# we can suppress this warning as follows:
options(rgdal_show_exportToProj4_warnings = "none")



### Assemble spatial covariates as raster data ---------------------------------

# Data root
v_data_root <- here::here("data/covariates")

# Read from all covariate folders (only tif related files, e.g. no text metadata files)
# for now also only covariates at 1km resolution
# \\ = escape special character (.); $ = end of string
v_dirs_rec <- dirname(dir(here::here("data/covariates"),
                        pattern = "1km\\.tif$", recursive = TRUE))

v_dirs_nonrec <- dirname(dir(here::here("data/covariates")))

# List folders with covariate data
(v_folder_names <- rlang::set_names(
  unique(v_dirs_rec[!v_dirs_rec %in% v_dirs_nonrec])
))

# Paths where files are located
(ls_folder_paths <- purrr::map(.x = v_folder_names,
                            .f = ~ paste(v_data_root, .x, sep = "/")
                            ))

# vector of covariate files (recursive path)
v_files_rec <- map(ls_folder_paths, ~ dir(path = .x, pattern = "1km\\.tif$",
                                          full.names = TRUE)) %>%
  unlist() %>% 
  unname()

# vector of covariate files (non-recursive; file names)
v_files_nonrec <- map(ls_folder_paths, ~ dir(path = .x, pattern = "1km\\.tif$",
                                          full.names = FALSE)) %>%
  unlist() %>% 
  unname()

# get filenames without filetype (.tif)
v_files_names <- v_files_nonrec %>% 
  str_replace(., ".tif", "")

# read in all covariates seperately without making a stack
# use stack() instead of raster() so that all bands are read of multiband GeoTIFFs
ls_r_cov <- map(v_files_rec, ~stack(.x)) %>%
  set_names(., v_files_names) %>% # name list using covariate names
  map(., ~unstack(.x)) %>% # unstack to make bands individual rasters
  unlist()
# (was unable to easily convert this code using "terra" instead of "raster")

# vector of better names for individual bands of multiband covariates
v_cov_names <- foreach(i = 1:length(ls_r_cov)) %do% {
  ifelse(grepl("1km.1", names(ls_r_cov[[i]])),
         str_replace(names(ls_r_cov[[i]]), "RGBNIR", "R"),
         ifelse(grepl("1km.2", names(ls_r_cov[[i]])),
                str_replace(names(ls_r_cov[[i]]), "RGBNIR", "G"),
                ifelse(grepl("1km.3", names(ls_r_cov[[i]])),
                       str_replace(names(ls_r_cov[[i]]), "RGBNIR", "B"),
                       ifelse(grepl("1km.4", names(ls_r_cov[[i]])),
                              str_replace(names(ls_r_cov[[i]]), "RGBNIR", "NIR"),
                              names(ls_r_cov[[i]])))))
} %>% 
  unlist() %>% 
  str_replace(., "1km.\\d", "1km")

# rename with proper names
ls_r_cov <- ls_r_cov %>% 
  set_names(., v_cov_names)

# Total number of covariates and their names
length(ls_r_cov)
summary(ls_r_cov)

# Briefly plot some (unprepared) covariates for a quick glance:
# 1) A continuous covariate (AHN will be used as reference for resampling)
# plot(ls_r_cov$ahn3_1km)

# 2) A multiband covariate (R, G, & B bands of S2 monthly mosaic)
# Stretch values to extend to full 0-255 range to increase visual contrast
# plotRGB(stack(ls_r_cov$s2_RGBNIR_2016_09sep_r_1km,
#               ls_r_cov$s2_rgbnir2016_09sep_g_1km,
#               ls_r_cov$s2_rgbnir2016_09sep_b_1km),
#         stretch = "lin") # "hist"

# 3) A categorical covariate (e.g. crop rotation data)
# plot(ls_r_cov$brp_gewaspercelen2005_cat_1km$brp_gewaspercelen2005_cat_1km,
#      col = bpy.colors())

# clean up: remove temporary variables and data
rm(ls_folder_paths, pkgs, v_data_root, v_dirs_nonrec, v_dirs_rec,
   v_files_names, v_files_nonrec, v_files_rec, v_folder_names)



### Check CRS, origin & extent to determine which covariates to resample -------

## check coordinate reference system (CRS)
crs_all_cov <- map(ls_r_cov, ~crs(.x)) %>% 
  unlist() %>% 
  as.character()

# Are CRS the same
unique(crs_all_cov)

# designate correct CRS to all covariates
for (rr in 1:length(ls_r_cov)) {
  # browser()
  crs(ls_r_cov[[rr]]) <- "EPSG:28992"
}
# all rasters now have the same CRS

# check origin
map(ls_r_cov, ~origin(.x)) %>% 
  unique()
# not all covariates have the same origin

# check extent
map(ls_r_cov, ~extent(.x)) %>% 
  unique()
# not all covariates have the same extent

# resample to get the same origin and extent (resolution is identical for all)
# save to disk for resampling in GDAL without using precious Rsession memory space
foreach(rr = 1:length(ls_r_cov)) %do%
  raster::writeRaster(ls_r_cov[[rr]],
                      paste0("out/data/covariates/resample/",
                             names(ls_r_cov[rr]), ".tif"),
                      overwrite = TRUE)



### Resample covariates using GDAL ------------------------------------------

# Resample using GDAL and run it externally on system
# the following code is from Hengl & MacMillan's "Predictive Soil Mapping with R"
if(.Platform$OS.type == "windows"){
  gdal.dir <- shortPathName("C:/Program Files/GDAL")
  gdal_translate <- paste0(gdal.dir, "/gdal_translate.exe")
  gdalwarp <- paste0(gdal.dir, "/gdalwarp.exe")
  gdal_calc.py <- paste0(gdal.dir, "/gdalcalc.py.exe")
} else {
  gdal_translate = "gdal_translate"
  gdalwarp = "gdalwarp"
  gdal_calc.py = "gdal_calc.py"
}
system(paste(gdalwarp, "--help"))
#> Warning in system(paste(gdalwarp, "--help")): error in running command

# Use resampling method specific to whether covariate is categorical vs. continuous
# Categorical covariate: nearest neighbor ("near" in gdalwarp fnc)
# Continuous covariate: cubic spline ("cubicspline" in gdalwarp fnc)

# In order to make it specific to categorical/continuous read in metadata:
tbl_cov_meta <- read_csv("data/covariates/covariates_metadata.csv") %>% 
  # only metadata info of covariates we need to resample
  filter(name %in% names(ls_r_cov))

# make sure there are only continuous or categorical "values_type" (binary)
tbl_cov_meta$values_type %>% 
  unique()

# match row order of metadata to list of covariates
tbl_cov_meta <- tbl_cov_meta[match(names(ls_r_cov), tbl_cov_meta$name),]

# force format/datatype specific for each covariate to save disk space using "ot" argument
# For R raster pkg datatypes see ?dataType
# For gdal datatypes see https://grass.osgeo.org/grass78/manuals/r.out.gdal.html
# add column with metadata info cat/cont to resample specific to this
tbl_data_value_type_cov <- map(ls_r_cov, ~dataType(.x)) %>% 
  unlist() %>% 
  as_tibble() %>% 
  add_column(cov = names(ls_r_cov), .before = "value") %>% 
  add_column(value_type = tbl_cov_meta$values_type, .before = "value") %>% 
  mutate(resampling = case_when(value_type %in% "categorical" ~ "near",
                                value_type %in% "continuous" ~ "cubicspline"),
         .before = "value") %>% 
  rename(R_datatype = value) %>% 
  mutate(GDAL_datatype = case_when(R_datatype %in% "INT1U" ~ "Byte",
                                   R_datatype %in% "INT2S" ~ "Int16",
                                   R_datatype %in% "INT2U" ~ "UInt16",
                                   R_datatype %in% "INT4S" ~ "Int32",
                                   R_datatype %in% "FLT4S" ~ "Float32"))

# resample covariates using GDAL on OS
foreach(rr = 1:length(ls_r_cov)) %do%
  system(paste0('gdalwarp out/data/covariates/resample/',
                tbl_data_value_type_cov$cov[rr], '.tif',
                ' out/data/covariates/resample/',
                tbl_data_value_type_cov$cov[rr], '_resampled.tif -ot ',
                tbl_data_value_type_cov$GDAL_datatype[rr],
                ' -r \"', tbl_data_value_type_cov$resampling[rr], '\" -te ',
                paste(as.vector(raster::extent(ls_r_cov$ahn3_1km))[c(1,3,2,4)], collapse=" "),
                ' -tr ', raster::res(ls_r_cov$ahn3_1km)[1], ' ',
                raster::res(ls_r_cov$ahn3_1km)[2],
                ' -overwrite'))



### Create raster stack of covariates, add Northing & Easting ------------------

# Data root
v_data_root <- here::here("out/data/covariates/resample")

# resampled and masked covariate file names
ls_files <- list.files(path = v_data_root, pattern = "1km_resampled\\.tif$")

# resampled covariate file names including path
ls_file_paths <- map(ls_files, ~paste(v_data_root, .x, sep = "/"))

# create raster stack of resampled covariates
r_stack_cov <- stack(ls_file_paths)

# although all CRS are the same, the string needs to be identical
crs(r_stack_cov) <- "EPSG:28992"

# print crs according to PROJ 7 or higher as follows:
cat(wkt(r_stack_cov))

# create Northing and Easting matrices
tbl_East_North <- coordinates(r_stack_cov) %>% 
  as_tibble() %>% 
  mutate(Easting = x, Northing = y)

# Turn into rasters as additional geographical covariates
r_Easting <- rasterFromXYZ(xyz = tbl_East_North %>% 
                             dplyr::select(x, y, Easting),
                           res = res(r_stack_cov),
                           crs = crs(r_stack_cov))

r_Northing <- rasterFromXYZ(xyz = tbl_East_North %>% 
                              dplyr::select(x, y, Northing),
                            res = res(r_stack_cov),
                            crs = crs(r_stack_cov))

# Save newly created raster layers to disk to prevent using precious RAM
r_Easting <- writeRaster(r_Easting,
                         "out/data/covariates/easting_northing/easting_1km.tif",
                         overwrite = TRUE,
                         datatype = "INT4S")
r_Northing <- writeRaster(r_Northing,
                          "out/data/covariates/easting_northing/northing_1km.tif",
                          overwrite = TRUE,
                          datatype = "INT4S")

# combine all covariates: resampled ones, Easting and Northing
r_stack_cov <- stack(c(r_stack_cov, r_Easting, r_Northing))

# remove "resampled" in raster names
names(r_stack_cov) <- names(r_stack_cov) %>% 
  stringr::str_replace(., "_resampled", "")



### Mask continuous covariates -------------------------------------------------

# In order to make it specific to continuous read in metadata:
tbl_cov_meta <- read_csv("data/covariates/covariates_metadata.csv") %>% 
  # only metadata info of covariates we need to resample
  filter(name %in% names(r_stack_cov))

# retrieve names of continuous covariates
v_cont <- tbl_cov_meta %>% 
  filter(values_type %in% "continuous") %>% 
  .$name

# check out values of mask (values need to be reversed)
r_mask_rev <- raster("data/other/ahn3_dtm_nodata_1km.tif")

unique(values(r_mask_rev)) # change so that 1 -> 0 and NA -> 1

# create raster with value = 0
r_val0 <- setValues(r_mask_rev, 0)

# create raster mask: 0 = water and outside NL, 1 = target mapping pixels
r_mask <- mask(x = r_val0,
               mask = r_mask_rev,
               maskvalue = 0, # for ahn3 mask change maskvalue arg from 0 to NA
               updatevalue = 1)

# Save newly created raster to disk to prevent using precious RAM
r_mask <- writeRaster(r_mask,
                      "out/data/covariates/mask_continuous/ahn3_dtm_nodata_mask_1km.tif",
                      overwrite = TRUE)

# mask continuous covariates
ls_r_cov_cont_masked <- foreach(cont = 1:length(v_cont)) %do% {
  raster::mask(x = r_stack_cov[[v_cont[cont]]],
               mask = r_mask,
               maskvalue = 0) 
}

# save masked rasters (in Rsession memory) to disk
foreach(cont = 1:length(ls_r_cov_cont_masked)) %do%
  raster::writeRaster(ls_r_cov_cont_masked[[cont]],
                      paste0("out/data/covariates/mask_continuous/",
                             v_cont[cont], ".tif"),
                      overwrite = TRUE)

# read in saved rasters
ls_r_cov_cont_masked <- foreach(cont = 1:length(v_cont)) %do%
  raster::raster(paste0("out/data/covariates/mask_continuous/",
                        v_cont[cont], ".tif"))

# replace continuous rasters with new masked ones
r_stack_cov <- dropLayer(r_stack_cov, v_cont)
r_stack_cov <- stack(r_stack_cov, stack(ls_r_cov_cont_masked))



### plot some covariates, save them & clean up ---------------------------------

# get an idea of 2D extent, as well as number of layers (rasters)
dim(r_stack_cov)

# plot a few rasters as a quick quality check
plot(r_stack_cov[[25]], main = names(r_stack_cov[[25]]))
plot(r_stack_cov[[1]], main = names(r_stack_cov[[1]]))

# save raster stack using saveStack (only saves references to files written on disk)
stackSave(r_stack_cov, "out/data/covariates/20_r_stack_cov.grd")

# clean up: remove old, not resampled, orginal files on disk
unlink(paste0("out/data/covariates/resample/",
              grep(list.files(path = "out/data/covariates/resample/"),
                   pattern = "_resampled",
                   invert = TRUE,
                   value = TRUE)))


