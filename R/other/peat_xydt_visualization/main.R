## Author:   Luc Steinbuch (SGL)
## see also: https://git.wageningenur.nl/stein012/2022_07_visualising_peat_change


## Visualising peat categories with one peat horizon in different start- and ending depths,
## including the difference between two categories


# No libraries needed :-)


# Set working directory to current folder (works only in RStudio)
if (Sys.getenv("RSTUDIO") == "1")
{
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
}  


## Dataframe with 8 categories; for each category, the most shallow (s) and 
#  deepest (d) options are provided
#  Note: category 9, "no determination possible", is not in this system

df_peat_horizons <- data.frame(category = 1:8)

df_peat_horizons$shallow_start_peat <- 
  c(0, 0, 15, 15, 40, 40, 80, 120 )
#   1s 2s 3s  4s  5s  6s  7s   8s

df_peat_horizons$shallow_end_peat <- 
  c(40, 15, 55, 30, 80, 55, 120, 120)
#   1s  2s   3s  4s  5s  6s  7s   8s

df_peat_horizons$deep_start_peat <- 
  c(15, 15, 40, 40, 80, 80, 120, 120)
#   1d  2d  3d  4d  5d  6d  7d    8d

df_peat_horizons$deep_end_peat <- 
  c(55, 55, 80, 80, 120, 120, 160, 120)
#   1d  2d  3d  4d   5d   6d   7d   8d

df_peat_horizons$rest_perhaps_peat <- # Or in other words: perhaps peat
  c(TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE)
#     1      2    3      4      5     6     7      8


(df_peat_horizons)

## The main function, plots the two categories, their extremes and the comparison
source("compare_two_peat_categories.R")

## Supporting function for compare_two_peat_categories
source("plot_peat_horizons.R")

compare_two_peat_categories(n_catA = 1, 
                            n_catB = 1)


