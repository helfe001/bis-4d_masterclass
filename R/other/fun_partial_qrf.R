# Partial Dependence Plot for Quantile Random Forest

# for dependency of any quantile in QRF, we can use adjusted fnc based on:
# https://github.com/vlyubchich/MML/blob/master/R/partial_qrf.R
partial_qrf <- function(object, pred.var
                        ,Q = c(0.05, 0.5, 0.95)
                        ,...)
{
  Q <- sort(unique(Q))
  predfun <- function(object, newdata) {
    qpred <- predict(object, newdata, type = "quantiles", quantiles = Q)$predictions
    tmp <- c(apply(qpred, 2, mean))
    # "q_temp" was chosen b/c should be name that doesn't occur in R environment
    # see https://stackoverflow.com/questions/44702134/r-error-cannot-change-value-of-locked-binding-for-df
    q_temp <- paste0("q", Q)
    q_temp <<- q_temp <- gsub("\\.", "_", q_temp)
    names(tmp) <- q_temp
    return(tmp)
  }
  pdpout <- pdp::partial(object
                         ,pred.var = pred.var
                         ,pred.fun = predfun
                         ,...)
  if ("call" %in% ls(object)) {
    response <- as.character(object$call)
    response <- base::strsplit(response[2], "\\ ")[[1]][1]
    colnames(pdpout)[length(pred.var) + 1] <- response
  }
  if (length(Q) > 1) {
    colnames(pdpout)[ncol(pdpout)] <- "pred"
    pdpout$pred <- factor(pdpout$pred, levels = rev(q_temp))
  }
  return(pdpout)
}